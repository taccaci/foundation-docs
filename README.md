iPlant Foundation API Documentation
==============================================

> Note: This is updated in sync with official releases of the API. Future versions of the API will auto-generate their own documentation and the json files included in this project will be removed. For more information or to view the live documentation, please visit. [https://foundation.iplantcollaborative.org/v2/docs](https://foundation.iplantcollaborative.org/v2/docs).

This is the live documentation for the iPlant Foundation API. The documentation is authored against the [Swagger](https://github.com/wordnik/swagger-core/wiki) specification and the UI is automagically generated from the documentation. 

Installation
---------------

Drop the folder as-is into your web server. Updated the resources.json file to point to the URL of the webapp on your server.

