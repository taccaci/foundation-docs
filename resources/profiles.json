	{
  "apiVersion": "2.0",
  "swaggerVersion": "1.1",
  "basePath": "https://iplant-dev.tacc.utexas.edu/v2/",
  "resourcePath": "/profiles",
  "apis":[
	  {
	    "path":"/",
	    "description":"Retrieve the profile of the requesting user.",
	    "operations":[
	      {
	        "httpMethod":"GET",
	        "nickname":"getMyProfile",
	        "responseClass":"MultipleProfileResponse",
	        "parameters":[ ],
	        "summary":"Find authenticated user profile",
	        "notes": "",
	        "errorResponses":[ 
	        	{
					"code": 400,
    				"reason": "Raised if a user supplies an invalid username format"
  				},
  				{
					"code": 403,
    				"reason": "Failed to authenticate the user"
  				},
  				{
    				"code": 404,
    				"reason": "The user profile cannot be found"
  				},
  				{
    				"code": 500,
    				"reason": "The service was unable to query the profile database"
  				}
	        ]
	      }
	    ]
	  },
	  {
	    "path":"/{username}",
	    "description":"Retrieve the profile of an API user by their exact username.",
	    "operations":[
	      {
	        "httpMethod":"GET",
	        "nickname":"getProfileByUsername",
	        "responseClass":"MultipleProfileResponse",
	        "parameters":[
	        	{
            		"paramType": "path",
		            "name": "username",
		            "description": "The username of a valid api user",
		            "dataType": "string",
		            "required": true,
		            "allowMultiple": false
          		}
	        ],
	        "summary":"Find api user profile by their api username",
	        "notes": "Internal users will not be returned",
	        "errorResponses":[ 
	        	{
					"code": 400,
    				"reason": "Raised if a user supplies an invalid username format"
  				},
  				{
					"code": 403,
    				"reason": "Failed to authenticate the user"
  				},
  				{
    				"code": 404,
    				"reason": "The user profile cannot be found"
  				},
  				{
    				"code": 500,
    				"reason": "The service was unable to query the profile database"
  				}
	        ]
	      }
	    ]
	  },
	  {
	    "path":"/search/{term}/{value}",
	    "description":"Retrieve the profile of the requesting user.",
	    "operations":[
	      {
	        "httpMethod":"GET",
	        "nickname":"getMyProfile",
	        "responseClass":"MultipleProfileResponse",
	        "parameters":[
	        	{
            		"paramType": "path",
		            "name": "term",
		            "description": "The profile attribute by which to search.",
		            "dataType": "string",
		            "required": true,
		            "allowMultiple": false,
		            "allowableValues":{
			        	"valueType":"LIST",
			        	"values":[
				            "username",
				            "email",
				            "name"
			          ]
			        }
          		},
	        	{
            		"paramType": "path",
		            "name": "value",
		            "description": "The search value. Partial matches will be returned.",
		            "dataType": "string",
		            "required": true,
		            "allowMultiple": false
          		} 
          	],
	        "summary":"Search for authenticated user profiles by search terms",
	        "notes": "All searches will be done using partial matches.",
	        "errorResponses":[ ]
	      }
	    ]
	  },
	  {
	    "path":"/{username}/users",
	    "description":"Perform operations on internal users",
	    "operations":[
	      {
	        "httpMethod":"GET",
	        "nickname":"getAllInternalUsers",
	        "responseClass":"MultipleInternalUserResponse",
	        "parameters":[ 
	        	{
            		"paramType": "path",
		            "name": "username",
		            "description": "The username of a valid api user",
		            "dataType": "string",
		            "required": true,
		            "allowMultiple": false
          		}
          	],
	        "summary":"List all internal users created by the authenticated user",
	        "notes": "",
	        "errorResponses":[ ]
	      },
	      {
	        "httpMethod":"DELETE",
	        "nickname":"deleteInternalUser",
	        "responseClass":"SingleInternalUserResponse",
	        "parameters":[ 
	        	{
            		"paramType": "path",
		            "name": "username",
		            "description": "The username of a valid api user",
		            "dataType": "string",
		            "required": true,
		            "allowMultiple": false
          		}
          	],
	        "summary":"Delete all internal users created by the authenticated user.",
	        "notes": "Deleted users cannot be recovered.",
	        "errorResponses":[ ]
	      },
	      {
	        "httpMethod":"POST",
	        "nickname":"createUpdateInternalUser",
	        "responseClass":"SingleInternalUserResponse",
	        "parameters":[
	        	{
            		"paramType": "path",
		            "name": "username",
		            "description": "The username of a valid api user",
		            "dataType": "string",
		            "required": true,
		            "allowMultiple": false
          		},
          		{
            		"paramType": "form",
		            "name": "username",
		            "description": "The internal user's username. This must be unique among all internal users created by the authenticated api user.",
		            "dataType": "string",
		            "required": true,
		            "allowMultiple": false
          		},
          		{
            		"paramType": "form",
		            "name": "email",
		            "description": "The internal user's email address. This must be unique among all internal users created by the authenticated api user.",
		            "dataType": "string",
		            "required": true,
		            "allowMultiple": false
          		},
          		{
            		"paramType": "form",
		            "name": "firstName",
		            "description": "The internal user's first name.",
		            "dataType": "string",
		            "required": false,
		            "allowMultiple": false
          		},
          		{
            		"paramType": "form",
		            "name": "lastName",
		            "description": "The internal user's last name.",
		            "dataType": "string",
		            "required": false,
		            "allowMultiple": false
          		},
          		{
            		"paramType": "form",
		            "name": "position",
		            "description": "The internal user's position at their institution.",
		            "dataType": "string",
		            "required": false,
		            "allowMultiple": false
          		},
          		{
            		"paramType": "form",
		            "name": "institution",
		            "description": "The internal user's institution.",
		            "dataType": "string",
		            "required": false,
		            "allowMultiple": false
          		},
          		{
            		"paramType": "form",
		            "name": "phone",
		            "description": "The internal user's phone number in ###-###-#### format.",
		            "dataType": "string",
		            "required": false,
		            "allowMultiple": false
          		},
          		{
            		"paramType": "form",
		            "name": "fax",
		            "description": "The internal user's phone number in ###-###-#### format.",
		            "dataType": "string",
		            "required": false,
		            "allowMultiple": false
          		},
          		{
            		"paramType": "form",
		            "name": "researchArea",
		            "description": "The internal user's first name.",
		            "dataType": "string",
		            "required": false,
		            "allowMultiple": false
          		},
          		{
            		"paramType": "form",
		            "name": "department",
		            "description": "The internal user's first name.",
		            "dataType": "string",
		            "required": false,
		            "allowMultiple": false
          		},
          		{
            		"paramType": "form",
		            "name": "city",
		            "description": "The internal user's first name.",
		            "dataType": "string",
		            "required": false,
		            "allowMultiple": false
          		},
          		{
            		"paramType": "form",
		            "name": "state",
		            "description": "The internal user's state.",
		            "dataType": "string",
		            "required": false,
		            "allowMultiple": false
          		},
          		{
            		"paramType": "form",
		            "name": "country",
		            "description": "The internal user's country.",
		            "dataType": "string",
		            "required": false,
		            "allowMultiple": false
          		},
          		{
            		"paramType": "form",
		            "name": "gender",
		            "description": "The internal user's gender.",
		            "dataType": "string",
		            "required": false,
		            "allowMultiple": false,
		            "allowableValues":{
			        	"valueType":"LIST",
			        	"values":[
				            "male",
				            "female"
			          	]
			        }
          		}
	        ],
	        "summary":"Create or update an internal user.",
	        "notes": "",
	        "errorResponses":[ ]
	      }
	    ]
	  },
	  {
	    "path":"/{username}/users/{internalUsername}",
	    "description":"Perform operations on a specific internal user",
	    "operations":[
	      {
	        "httpMethod":"GET",
	        "nickname":"getInternalUserByUsername",
	        "responseClass":"SingleInternalUserResponse",
	        "parameters":[
	        	{
            		"paramType": "path",
		            "name": "username",
		            "description": "The username of a valid api user",
		            "dataType": "string",
		            "required": true,
		            "allowMultiple": false
          		},
          		{
            		"paramType": "path",
		            "name": "internalUsername",
		            "description": "The username of a valid internal user",
		            "dataType": "string",
		            "required": true,
		            "allowMultiple": false
          		}
          	],
	        "summary":"Find api user profile by their api username",
	        "notes": "",
	        "errorResponses":[ ]
	      },
	      {
	        "httpMethod":"DELETE",
	        "nickname":"deleteInternalUser",
	        "responseClass":"SingleInternalUserResponse",
	        "parameters":[ 
	        	{
            		"paramType": "path",
		            "name": "username",
		            "description": "The username of a valid api user",
		            "dataType": "string",
		            "required": true,
		            "allowMultiple": false
          		},
          		{
            		"paramType": "path",
		            "name": "internalUsername",
		            "description": "The username of a valid internal user",
		            "dataType": "string",
		            "required": true,
		            "allowMultiple": false
          		}
          	],
	        "summary":"Delete all internal users created by the authenticated user.",
	        "notes": "Deleted users cannot be recovered.",
	        "errorResponses":[ ]
	      },
	      {
	        "httpMethod":"POST",
	        "nickname":"createUpdateInternalUser",
	        "responseClass":"SingleInternalUserResponse",
	        "parameters":[
	        	{
            		"paramType": "path",
		            "name": "username",
		            "description": "The username of a valid api user",
		            "dataType": "string",
		            "required": true,
		            "allowMultiple": false
          		},
          		{
            		"paramType": "path",
		            "name": "internalUsername",
		            "description": "The username of a valid internal user",
		            "dataType": "string",
		            "required": true,
		            "allowMultiple": false
          		},
	        	{
            		"paramType": "form",
		            "name": "username",
		            "description": "The internal user's username. This must be unique among all internal users created by the authenticated api user.",
		            "dataType": "string",
		            "required": true,
		            "allowMultiple": false
          		},
          		{
            		"name": "email",
		            "paramType": "form",
		            "description": "The internal user's email address. This must be unique among all internal users created by the authenticated api user.",
		            "dataType": "string",
		            "required": true,
		            "allowMultiple": false
          		},
          		{
            		"paramType": "form",
		            "name": "firstName",
		            "description": "The internal user's first name.",
		            "dataType": "string",
		            "required": false,
		            "allowMultiple": false
          		},
          		{
            		"paramType": "form",
		            "name": "lastName",
		            "description": "The internal user's last name.",
		            "dataType": "string",
		            "required": false,
		            "allowMultiple": false
          		},
          		{
            		"paramType": "form",
		            "name": "position",
		            "description": "The internal user's position at their institution.",
		            "dataType": "string",
		            "required": false,
		            "allowMultiple": false
          		},
          		{
            		"paramType": "form",
		            "name": "institution",
		            "description": "The internal user's institution.",
		            "dataType": "string",
		            "required": false,
		            "allowMultiple": false
          		},
          		{
            		"paramType": "form",
		            "name": "phone",
		            "description": "The internal user's phone number in ###-###-#### format.",
		            "dataType": "string",
		            "required": false,
		            "allowMultiple": false
          		},
          		{
            		"paramType": "form",
		            "name": "fax",
		            "description": "The internal user's phone number in ###-###-#### format.",
		            "dataType": "string",
		            "required": false,
		            "allowMultiple": false
          		},
          		{
            		"paramType": "form",
		            "name": "researchArea",
		            "description": "The internal user's first name.",
		            "dataType": "string",
		            "required": false,
		            "allowMultiple": false
          		},
          		{
            		"paramType": "form",
		            "name": "department",
		            "description": "The internal user's first name.",
		            "dataType": "string",
		            "required": false,
		            "allowMultiple": false
          		},
          		{
            		"paramType": "form",
		            "name": "city",
		            "description": "The internal user's first name.",
		            "dataType": "string",
		            "required": false,
		            "allowMultiple": false
          		},
          		{
            		"paramType": "form",
		            "name": "state",
		            "description": "The internal user's state.",
		            "dataType": "string",
		            "required": false,
		            "allowMultiple": false
          		},
          		{
            		"paramType": "form",
		            "name": "country",
		            "description": "The internal user's country.",
		            "dataType": "string",
		            "required": false,
		            "allowMultiple": false
          		},
          		{
            		"paramType": "form",
		            "name": "gender",
		            "description": "The internal user's gender.",
		            "dataType": "string",
		            "required": false,
		            "allowMultiple": false,
		            "allowableValues":{
			        	"valueType":"LIST",
			        	"values":[
				            "male",
				            "female"
			          	]
          			}
          		}
	        ],
	        "summary":"Create or update the given internal user.",
	        "notes": "",
	        "errorResponses":[ ]
	      } 
	    ]
	  },
	  {
	    "path":"/{username}/users/search/{term}/{value}",
	    "description":"Retrieve the profile of the requesting user.",
	    "operations":[
	      {
	        "httpMethod":"GET",
	        "nickname":"getMyProfile",
	        "responseClass":"MultipleProfileResponse",
	        "parameters":[
	        	{
            		"paramType": "path",
		            "name": "username",
		            "description": "The username of a valid api user",
		            "dataType": "string",
		            "required": true,
		            "allowMultiple": false
          		},
          		{
            		"paramType": "path",
		            "name": "term",
		            "description": "The profile attribute by which to search. Status may be either active or deleted.",
		            "dataType": "string",
		            "required": true,
		            "allowMultiple": false,
		            "allowableValues":{
			        	"valueType":"LIST",
			        	"values":[
				            "username",
				            "email",
				            "name",
				            "status"
			          ]
			        }
          		},
	        	{
            		"paramType": "path",
		            "name": "value",
		            "description": "The search value. Partial matches will be returned.",
		            "dataType": "string",
		            "required": true,
		            "allowMultiple": false
          		} 
          	],
	        "summary":"Search for internal user profiles by search terms",
	        "notes": "All searches will be done using partial matches.",
	        "errorResponses":[ ]
	      }
	    ]
	  }
  ],
  "models": {
  	"Profile": {
  		"id":"Profile",
  		"properties":{
			"username":{
    			"type": "string",
    			"description": "The api user's unique username."
  			},
  			"email":{
    			"type": "string",
    			"description": "The api user's unique email address."
    		},
			"firstName":{
    			"type": "string",
    			"description": "The api user's first name."
    		},
			"lastName":{
    			"type": "string",
    			"description": "The api user's last name."
    		},
			"position":{
    			"type": "string",
    			"description": "The api user's position of employment."
    		},
			"institution":{
    			"type": "string",
    			"description": "The api user's home institution"
    		},
			"phone":{
    			"type": "string",
    			"description": "The api user's phone number."
    		},
			"fax":{
    			"type": "string",
    			"description": "The api user's fax number."
    		},
			"researchArea":{
    			"type": "string",
    			"description": "The api user's primary area of research."
    		},
			"department":{
    			"type": "string",
    			"description": "The api user's institutional department."
    		},
			"city":{
    			"type": "string",
    			"description": "The api user's city."
    		},
			"state":{
    			"type": "string",
    			"description": "The api user's state."
    		},
			"country":{
    			"type": "string",
    			"description": "The api user's country."
    		},
			"gender":{
    			"type": "string",
    			"description": "The api user's gender. male or female."
    		}
		}
	},
	"SingleProfileResponse": {
		"id": "SingleProfileResponse",
		"properties": {
			"status": {
				"type":"string",
				"description": "success or failure"
			},
			"message": {
				"type":"string",
				"description": "success or failure"
			},
			"result": {
				"type":"Profile"
			}
		}
	},
	"MultipleProfileResponse": {
		"id": "MultipleProfileResponse",
		"properties": {
			"status": {
				"type":"string",
				"description": "success or failure"
			},
			"message": {
				"type":"string",
				"description": "success or failure"
			},
			"result": {
				"type":"List",
				"description": "response body",
				"items": {
					"$ref":"Profile"
				}
			}
		}
	},
	"InternalUser": {
  		"id":"InternalUser",
  		"properties":{
			"username":{
    			"type": "string",
    			"description": "The api user's unique username."
  			},
  			"email":{
    			"type": "string",
    			"description": "The api user's unique email address."
    		},
			"firstName":{
    			"type": "string",
    			"description": "The api user's first name."
    		},
			"lastName":{
    			"type": "string",
    			"description": "The api user's last name."
    		},
			"position":{
    			"type": "string",
    			"description": "The api user's position of employment."
    		},
			"institution":{
    			"type": "string",
    			"description": "The api user's home institution"
    		},
			"phone":{
    			"type": "string",
    			"description": "The api user's phone number."
    		},
			"fax":{
    			"type": "string",
    			"description": "The api user's fax number."
    		},
			"researchArea":{
    			"type": "string",
    			"description": "The api user's primary area of research."
    		},
			"department":{
    			"type": "string",
    			"description": "The api user's institutional department."
    		},
			"city":{
    			"type": "string",
    			"description": "The api user's city."
    		},
			"state":{
    			"type": "string",
    			"description": "The api user's state."
    		},
			"country":{
    			"type": "string",
    			"description": "The api user's country."
    		},
			"gender":{
    			"type": "string",
    			"description": "The api user's gender. male or female."
    		},
    		"status":{
    			"type": "string",
    			"description": "The api user's status. active or deleted."
    		}
		}
	},
	"SingleInternalUserResponse": {
		"id": "SingleInternalUserResponse",
		"properties": {
			"status": {
				"type":"string",
				"description": "success or failure"
			},
			"message": {
				"type":"string",
				"description": "success or failure"
			},
			"result": {
				"type":"InternalUser"
			}
		}
	},
	"MultipleInternalUserResponse": {
		"id": "MultipleInternalUserResponse",
		"properties": {
			"status": {
				"type":"string",
				"description": "success or failure"
			},
			"message": {
				"type":"string",
				"description": "success or failure"
			},
			"result": {
				"type":"List",
				"description": "response body",
				"items": {
					"$ref":"InternalUser"
				}
			}
		}
	}
  }
}